include "dependencies.lua"

workspace "uzl2d"
    architecture "x86_64"
    startproject "editor"
    cppdialect "C++20"
    staticruntime "on"

    --location "build"
	debugdir "%{wks.location}"

    configurations
    {
        "debug",
        "release",
        "distribution"
    }

    flags
    {
        "MultiProcessorCompile"
    }

    defines
    {
        "_CRT_SECURE_NO_WARNINGS"
    }

    filter "system:windows"
		systemversion "latest"

    filter "configurations:debug"
        defines "UZ_DEBUG"
        optimize "Off"
        symbols "On"
        runtime "Debug"
    
    filter "configurations:release"
        defines "UZ_RELEASE"
        optimize "On"
        symbols "On"
        runtime "Release"

    filter "configurations:distribution"
        defines "UZ_DISTRIBUTION"
        optimize "Speed"
        symbols "Off"
        runtime "Release"

        bin_dir = "%{wks.location}/bin/%{cfg.buildcfg}/"
        obj_dir = "%{wks.location}/obj/%{cfg.buildcfg}/"

group "core"
    include "engine"
    include "editor"
group ""

group "third-party"
    include "third-party/glfw"
    include "third-party/stb"
group ""
