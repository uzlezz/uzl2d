VULKAN_SDK = os.getenv("VULKAN_SDK")



headers = {}

headers["glm"] = "%{wks.location}/third-party/glm"
headers["glfw"] = "%{wks.location}/third-party/glfw/include"
headers["stb"] = "%{wks.location}/third-party/stb/include"

headers["engine"] = "%{wks.location}/engine/include"



library = {}