project "stb"
    kind "StaticLib"
    language "C++"

    targetdir (bin_dir .. "third-party/%{prj.name}")
    objdir (obj_dir .. "third-party/%{prj.name}")

    files
    {
        "include/**.h",
        "src/impl.cpp"
    }

    includedirs
    {
        "include",
    }