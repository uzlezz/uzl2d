project "engine"
    kind "StaticLib"
    language "C++"

    targetdir (bin_dir)
    objdir (obj_dir .. "%{prj.name}")

    files
    {
        "include/**.h",
        "src/**.cpp"
    }

    includedirs
    {
        "include",
        headers["glm"],
        headers["glfw"],
        headers["stb"]
    }

    links
    {
        "glfw",
        "stb"
    }