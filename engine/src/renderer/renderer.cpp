#define GLFW_INCLUDE_NONE
#define VMA_IMPLEMENTATION
#include "renderer/renderer.h"
#include "renderer/texture.h"
#include "renderer/pipeline.h"
#include "utils.h"
#include "fs/virtual_file_system.h"
#include <GLFW/glfw3.h>
#include <stdexcept>
#include <vector>
#include <cstring>
#include <iostream>
#include <map>
#include <optional>
#include <set>
#include <array>
#include <filesystem>

namespace uz
{

    VkResult createDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger);
    void destroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator);

    Device::Device() {
        glfwInit();

        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
        gladLoadVulkanUserPtr(nullptr, (GLADuserptrloadfunc)glfwGetInstanceProcAddress, nullptr);
        m_window = glfwCreateWindow(1600, 900, "Magma lox", nullptr, nullptr);

        createInstance();
        createSurface();
        pickPhysicalDevice();
        
        if (!gladLoadVulkanUserPtr(m_physical_device, (GLADuserptrloadfunc)glfwGetInstanceProcAddress, m_instance)) {
            throw std::runtime_error("Cannot load vulkan");
        }
        
        createLogicalDevice();
        createAllocator();
        createSwapChain();

        createCommandPool();
        createCommandBuffers();
        createSyncObjects();

        createDescriptors();
        setupBindlessDescriptors();
        createGraphicsPipeline();
    }

    Device::~Device() {
        vkDeviceWaitIdle(m_device);

        for (u32 i = 0; i < m_num_frames_in_flight; ++i) {
            vkDestroySemaphore(m_device, m_image_available_semaphore[i], nullptr);
            vkDestroySemaphore(m_device, m_render_finished_semaphore[i], nullptr);
            vkDestroyFence(m_device, m_in_flight_fence[i], nullptr);
        }

        vkDestroyCommandPool(m_device, m_command_pool, nullptr);

        m_graphics_pipeline.reset();

        m_textures_manager.reset();
        vkDestroyDescriptorPool(m_device, m_descriptor_pool, nullptr);
        vkDestroyDescriptorSetLayout(m_device, m_descriptor_set_layout, nullptr);

        m_swap_chain.reset();
        vmaDestroyAllocator(m_allocator);
        vkDestroyDevice(m_device, nullptr);

        if constexpr (debug) {
            destroyDebugUtilsMessengerEXT(m_instance, m_debug_messenger, nullptr);
        }

        vkDestroySurfaceKHR(m_instance, m_surface, nullptr);
        vkDestroyInstance(m_instance, nullptr);

        glfwDestroyWindow(m_window);

        glfwTerminate();
    }

    bool Device::windowShouldClose() const {
        return glfwWindowShouldClose(m_window);
    }

    static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
            VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
            VkDebugUtilsMessageTypeFlagsEXT messageType,
            const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
            void* pUserData) {

        std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;

        return VK_FALSE;
    }

    static const std::array s_device_extensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME, VK_KHR_DYNAMIC_RENDERING_EXTENSION_NAME, VK_KHR_BUFFER_DEVICE_ADDRESS_EXTENSION_NAME };

    void Device::createInstance() {
        VkApplicationInfo app_info{ VK_STRUCTURE_TYPE_APPLICATION_INFO };
        app_info.pApplicationName = "UZL2D Game";
        app_info.pEngineName = "UZL2D Engine";
        app_info.apiVersion = VK_MAKE_VERSION(1, 3, 0);
        app_info.applicationVersion = VK_MAKE_VERSION(0, 0, 1);
        app_info.engineVersion = VK_MAKE_VERSION(0, 0, 1);

        VkInstanceCreateInfo instance_info{ VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO };
        instance_info.pApplicationInfo = &app_info;

        constexpr bool enable_validation_layers = debug;

        u32 glfw_extension_count = 0;
        const char** glfw_extensions = glfwGetRequiredInstanceExtensions(&glfw_extension_count);

        std::vector<const char*> required_extensions;

        for(uint32_t i = 0; i < glfw_extension_count; i++) {
            required_extensions.emplace_back(glfw_extensions[i]);
        }

        required_extensions.emplace_back(VK_KHR_PORTABILITY_ENUMERATION_EXTENSION_NAME);

        if constexpr (enable_validation_layers) {
            required_extensions.emplace_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        }

        instance_info.flags |= VK_INSTANCE_CREATE_ENUMERATE_PORTABILITY_BIT_KHR;

        instance_info.enabledExtensionCount = (uint32_t) required_extensions.size();
        instance_info.ppEnabledExtensionNames = required_extensions.data();

        const std::vector<const char*> validation_layers = {
                "VK_LAYER_KHRONOS_validation"
        };

        if constexpr (enable_validation_layers) {
            for (auto layer : validation_layers) {
                if (!checkValidationLayerSupport(layer)) {
                    throw std::runtime_error(std::string("Cannot enable validation layer ") + layer);
                }
            }

            instance_info.enabledLayerCount = static_cast<u32>(validation_layers.size());
            instance_info.ppEnabledLayerNames = validation_layers.data();
        }

        if (vkCreateInstance(&instance_info, nullptr, &m_instance) != VK_SUCCESS)
        {
            throw std::runtime_error("Cannot create vulkan instance");
        }

        VkDebugUtilsMessengerCreateInfoEXT messenger_info{ VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT };
        messenger_info.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT
                | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT
                | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
                | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
        messenger_info.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
                | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
                | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
        messenger_info.pfnUserCallback = debugCallback;
        messenger_info.pUserData = nullptr;

        if constexpr (debug) {
            if (createDebugUtilsMessengerEXT(m_instance, &messenger_info, nullptr, &m_debug_messenger) !=
                VK_SUCCESS) {
                throw std::runtime_error("Cannot create debug messenger util");
            }
        }
    }

    bool Device::checkValidationLayerSupport(const char* layer) {
        static u32 layer_count;
        static std::vector<VkLayerProperties> available_layers;
        if (layer_count == 0) {
            vkEnumerateInstanceLayerProperties(&layer_count, nullptr);
            available_layers.resize(layer_count);
            vkEnumerateInstanceLayerProperties(&layer_count, available_layers.data());
        }

        for (const auto& layer_props : available_layers) {
            if (strcmp(layer, layer_props.layerName) == 0) {
                return true;
            }
        }

        return false;
    }

    QueueFamilyIndices Device::findQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR surface) {
        QueueFamilyIndices indices{};

        u32 queue_family_count = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_family_count, nullptr);

        std::vector<VkQueueFamilyProperties> queue_families(queue_family_count);
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_family_count, queue_families.data());

        for (u32 i = 0; i < queue_families.size(); ++i) {
            const auto& family = queue_families[i];

            if (family.queueFlags & VK_QUEUE_GRAPHICS_BIT)
                indices.graphics_family = i;
            else if (family.queueFlags & VK_QUEUE_COMPUTE_BIT)
                indices.compute_family = i;


            if (indices.present_family.has_value()) continue;

            VkBool32 present_support = VK_FALSE;
            vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &present_support);

            if (present_support)
                indices.present_family = i;
        }

        return indices;
    }

    static bool checkDeviceExtensionsSupport(VkPhysicalDevice device) {
        u32 count = 0;
        vkEnumerateDeviceExtensionProperties(device, nullptr, &count, nullptr);

        std::vector<VkExtensionProperties> available_extensions(count);
        vkEnumerateDeviceExtensionProperties(device, nullptr, &count, available_extensions.data());

        std::set<std::string> required_extensions(s_device_extensions.begin(), s_device_extensions.end());

        for (const auto& extension : available_extensions) {
            required_extensions.erase(extension.extensionName);
        }

        return required_extensions.empty();
    }

    static i32 rateDeviceSuitability(VkPhysicalDevice device, VkSurfaceKHR surface) {
        VkPhysicalDeviceProperties properties;
        vkGetPhysicalDeviceProperties(device, &properties);

        VkPhysicalDeviceFeatures features;
        vkGetPhysicalDeviceFeatures(device, &features);

        VkPhysicalDeviceMemoryProperties mem;
        vkGetPhysicalDeviceMemoryProperties(device, &mem);

        auto indices = Device::findQueueFamilies(device, surface);
        if (!indices.graphics_family.has_value() || !indices.present_family.has_value()) {
            return -1;
        }

        if (!checkDeviceExtensionsSupport(device)) {
            return -1;
        }

        auto swap_chain_support_details = SwapChain::querySwapChainSupport(device, surface);
        if (swap_chain_support_details.formats.empty() || swap_chain_support_details.present_modes.empty()) {
            return -1;
        }

        i32 score = 0;

        score += (properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) * 1000;
        score += (properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU) * 100;
        score += (properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU) * 50;

        score += (properties.limits.maxImageDimension2D / 64);

        score += (indices.present_family.value() == indices.graphics_family.value()) * 50;

        score += indices.compute_family.has_value() * 500;

        return score;
    }

    void Device::pickPhysicalDevice() {
        u32 device_count = 0;
        vkEnumeratePhysicalDevices(m_instance, &device_count, nullptr);

        if (device_count == 0) {
            throw std::runtime_error("Cannot find GPUs with Vulkan support");
        }

        std::vector<VkPhysicalDevice> physical_devices(device_count);
        vkEnumeratePhysicalDevices(m_instance, &device_count, physical_devices.data());

        std::multimap<i32, VkPhysicalDevice> candidates;

        for (auto& physical_device : physical_devices) {
            candidates.insert(std::make_pair(rateDeviceSuitability(physical_device, m_surface), physical_device));
        }

        if (candidates.rbegin()->first >= 0) {
            m_physical_device = candidates.rbegin()->second;

            VkPhysicalDeviceProperties properties;
            vkGetPhysicalDeviceProperties(m_physical_device, &properties);

            m_properties.name = properties.deviceName;
            m_properties.max_texture2d_size = properties.limits.maxImageDimension2D;
        } else {
            throw std::runtime_error("failed to find a suitable GPU");
        }
    }

    void Device::createLogicalDevice() {
        auto indices = findQueueFamilies(m_physical_device, m_surface);

        float queue_priority = 1.0f;

        std::vector<VkDeviceQueueCreateInfo> queue_infos;

        VkDeviceQueueCreateInfo graphics_queue_info{ VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO };
        graphics_queue_info.queueFamilyIndex = indices.graphics_family.value();
        graphics_queue_info.queueCount = 1;
        graphics_queue_info.pQueuePriorities = &queue_priority;
        queue_infos.push_back(graphics_queue_info);

        if (indices.present_family.value() != indices.graphics_family.value()) {
            VkDeviceQueueCreateInfo present_queue_info{VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO };
            present_queue_info.queueFamilyIndex = indices.present_family.value();
            present_queue_info.queueCount = 1;
            present_queue_info.pQueuePriorities = &queue_priority;
            queue_infos.push_back(present_queue_info);
        }

        if (indices.compute_family.has_value()) {
            VkDeviceQueueCreateInfo compute_queue_info{ VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO };
            compute_queue_info.queueFamilyIndex = indices.compute_family.value();
            compute_queue_info.queueCount = 1;
            compute_queue_info.pQueuePriorities = &queue_priority;
            queue_infos.push_back(compute_queue_info);
        }

        VkPhysicalDeviceDescriptorIndexingFeatures indexing_features{};
        indexing_features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES;
        indexing_features.pNext = nullptr;

        VkPhysicalDeviceFeatures2 device_features2{};
        device_features2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
        device_features2.pNext = &indexing_features;

        // Fetch all features from physical device
        vkGetPhysicalDeviceFeatures2(m_physical_device, &device_features2);

        assert(indexing_features.shaderSampledImageArrayNonUniformIndexing);
        assert(indexing_features.descriptorBindingSampledImageUpdateAfterBind);
        assert(indexing_features.shaderUniformBufferArrayNonUniformIndexing);
        assert(indexing_features.descriptorBindingUniformBufferUpdateAfterBind);
        assert(indexing_features.shaderStorageBufferArrayNonUniformIndexing);
        assert(indexing_features.descriptorBindingStorageBufferUpdateAfterBind);

        VkPhysicalDeviceFeatures device_features{};

        VkPhysicalDeviceDynamicRenderingFeaturesKHR dynamic_rendering_feature{
            VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DYNAMIC_RENDERING_FEATURES_KHR,
            &device_features2,
            VK_TRUE
        };

        VkDeviceCreateInfo device_info{ VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO };
        device_info.pQueueCreateInfos = queue_infos.data();
        device_info.queueCreateInfoCount = static_cast<u32>(queue_infos.size());
        device_info.pEnabledFeatures = &device_features;

        device_info.enabledExtensionCount = static_cast<u32>(s_device_extensions.size());
        device_info.ppEnabledExtensionNames = s_device_extensions.data();
        device_info.pNext = &dynamic_rendering_feature;

        if (vkCreateDevice(m_physical_device, &device_info, nullptr, &m_device) != VK_SUCCESS) {
            throw std::runtime_error("Cannot create logical device");
        }

        vkGetDeviceQueue(m_device, indices.graphics_family.value(), 0, &m_graphics_queue);
        vkGetDeviceQueue(m_device, indices.present_family.value(), 0, &m_present_queue);
        if (indices.compute_family.has_value())
            vkGetDeviceQueue(m_device, indices.compute_family.value(), 0, &m_compute_queue);
    }

#define vkGetInstaceProcAddr_ glad_vkGetInstanceProcAddr
#define vkGetDeviceProcAddr_ glad_vkGetDeviceProcAddr
#undef vkGetInstanceProcAddr
#undef vkGetDeviceProcAddr

    void Device::createAllocator() {
        auto get_device_addr = glfwGetInstanceProcAddress(m_instance, "vkGetDeviceProcAddr");
        VmaVulkanFunctions vk_funcs{};
        vk_funcs.glad_vkGetInstanceProcAddr = &glfwGetInstanceProcAddress;
        vk_funcs.glad_vkGetDeviceProcAddr = (PFN_vkGetDeviceProcAddr)get_device_addr;

        VmaAllocatorCreateInfo allocator_info{};
        allocator_info.flags = 0;
        allocator_info.vulkanApiVersion = VK_API_VERSION_1_3;
        allocator_info.physicalDevice = m_physical_device;
        allocator_info.device = m_device;
        allocator_info.instance = m_instance;
        allocator_info.pVulkanFunctions = &vk_funcs;

        vmaCreateAllocator(&allocator_info, &m_allocator);
    }

#define vkGetInstanceProcAddr vkGetInstanceProcAddr_
#define vkGetDeviceProcAddr vkGetDeviceProcAddr_

    void Device::createSurface() {
        if (glfwCreateWindowSurface(m_instance, m_window, nullptr, &m_surface) != VK_SUCCESS)
            throw std::runtime_error("Cannot create surface");
    }

    void Device::createSwapChain() {
        m_swap_chain = std::make_unique<SwapChain>(*this);
    }

    void Device::setupBindlessDescriptors() {
        m_textures_manager = std::make_unique<BindlessTexturesManager>(*this, max_bindless_textures);
    }

    void Device::createGraphicsPipeline() {
        GraphicsPipelineSpecification spec;
        spec.alpha_blend = true;
        spec.vertex = "/shaders/textured_triangle.vert.spv";
        spec.fragment = "/shaders/textured_triangle.frag.spv";
        spec.set_layouts.push_back(m_descriptor_set_layout);

        m_graphics_pipeline = GraphicsPipeline::create(*this, spec);
    }

    void Device::createCommandPool() {
        const auto queue_family_indices = findQueueFamilies(m_physical_device, m_surface);

        VkCommandPoolCreateInfo pool_info{ VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO };
        pool_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
        pool_info.queueFamilyIndex = queue_family_indices.graphics_family.value();

        if (vkCreateCommandPool(m_device, &pool_info, nullptr, &m_command_pool) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create command pool");
        }
    }

    void Device::createCommandBuffers() {
        VkCommandBufferAllocateInfo alloc_info{ VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO };
        alloc_info.commandPool = m_command_pool;
        alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        alloc_info.commandBufferCount = m_num_frames_in_flight;
        m_frame_command_buffers.resize(m_num_frames_in_flight);

        if (vkAllocateCommandBuffers(m_device, &alloc_info, m_frame_command_buffers.data()) != VK_SUCCESS) {
            throw std::runtime_error("Failed to allocate frame command buffers");
        }
    }

    void Device::createSyncObjects() {
        VkSemaphoreCreateInfo semaphore_info{ VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO };

        VkFenceCreateInfo fence_info{ VK_STRUCTURE_TYPE_FENCE_CREATE_INFO };
        fence_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

        m_image_available_semaphore.resize(m_num_frames_in_flight);
        m_render_finished_semaphore.resize(m_num_frames_in_flight);
        m_in_flight_fence.resize(m_num_frames_in_flight);

        for (u32 i = 0; i < m_num_frames_in_flight; ++i) {
            if (vkCreateSemaphore(m_device, &semaphore_info, nullptr, &m_image_available_semaphore[i]) != VK_SUCCESS ||
                vkCreateSemaphore(m_device, &semaphore_info, nullptr, &m_render_finished_semaphore[i]) != VK_SUCCESS ||
                vkCreateFence(m_device, &fence_info, nullptr, &m_in_flight_fence[i]) != VK_SUCCESS) {
                throw std::runtime_error("Failed to create semaphores/fences");
            }
        }
    }

    void Device::createDescriptors() {
        VkDescriptorSetLayoutBinding texture_binding{};
        texture_binding.binding = 0;
        texture_binding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        texture_binding.descriptorCount = 1;
        texture_binding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
        texture_binding.pImmutableSamplers = nullptr;

        VkDescriptorSetLayoutCreateInfo layout_info{ VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO };
        layout_info.bindingCount = 1;
        layout_info.pBindings = &texture_binding;

        // TODO: Assert
        vkCreateDescriptorSetLayout(m_device, &layout_info, nullptr, &m_descriptor_set_layout);

        std::array<VkDescriptorPoolSize, 2> pool_sizes;
        pool_sizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        pool_sizes[0].descriptorCount = static_cast<u32>(1000);
        pool_sizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        pool_sizes[1].descriptorCount = static_cast<u32>(1000);

        VkDescriptorPoolCreateInfo pool_info{ VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO };
        pool_info.poolSizeCount = static_cast<u32>(pool_sizes.size());
        pool_info.pPoolSizes = pool_sizes.data();
        pool_info.maxSets = m_num_frames_in_flight;

        // TODO: Assert
        vkCreateDescriptorPool(m_device, &pool_info, nullptr, &m_descriptor_pool);

        std::vector layouts(m_num_frames_in_flight, m_descriptor_set_layout);
        VkDescriptorSetAllocateInfo alloc_info{ VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO };
        alloc_info.descriptorPool = m_descriptor_pool;
        alloc_info.descriptorSetCount = m_num_frames_in_flight;
        alloc_info.pSetLayouts = layouts.data();

        m_descriptor_sets.resize(m_num_frames_in_flight);
        // TODO: Assert
        vkAllocateDescriptorSets(m_device, &alloc_info, m_descriptor_sets.data());
    }

    void Device::beginRecordingCommandBuffer(VkCommandBuffer cmd_buffer) {
        VkCommandBufferBeginInfo begin_info{ VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
        begin_info.flags = 0;
        begin_info.pInheritanceInfo = nullptr;

        if (vkBeginCommandBuffer(cmd_buffer, &begin_info) != VK_SUCCESS) {
            throw std::runtime_error("Failed to begin recording command buffer");
        }
    }

    void Device::endRecordingCommandBuffer(VkCommandBuffer cmd_buffer) {
        if (vkEndCommandBuffer(cmd_buffer) != VK_SUCCESS) {
            throw std::runtime_error("Failed to record command buffer");
        }
    }

    VkResult createDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger) {
        auto func = (PFN_vkCreateDebugUtilsMessengerEXT)glad_vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
        if (func != nullptr)
            return func(instance, pCreateInfo, pAllocator, pDebugMessenger);

        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }

    void destroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator) {
        auto func = (PFN_vkDestroyDebugUtilsMessengerEXT) glad_vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
        if (func != nullptr)
            func(instance, debugMessenger, pAllocator);
    }




    std::shared_ptr<Texture2D> Device::createTexture2D(std::string_view path) {
        return Texture2D::create(*this, path);
    }

    void Device::createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VmaAllocationCreateFlags flags,
        VmaMemoryUsage mem_usage, VkMemoryPropertyFlags mem_props, VkBuffer& buffer, VmaAllocation& allocation) {
        VkBufferCreateInfo buffer_info{ VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
        buffer_info.size = size;
        buffer_info.usage = usage;
        buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

        VmaAllocationCreateInfo alloc_info{};
        alloc_info.flags = flags;
        alloc_info.usage = mem_usage;
        alloc_info.requiredFlags = mem_props;

        vmaCreateBuffer(m_allocator, &buffer_info, &alloc_info, &buffer, &allocation, nullptr);
    }

    void Device::createStagingBuffer(VkDeviceSize size, VkBuffer& buffer, VmaAllocation& allocation) {
        createBuffer(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
            VMA_MEMORY_USAGE_AUTO, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 
            buffer, allocation);
    }

    void Device::copyBuffer(VkBuffer src_buffer, VkDeviceSize src_offset,
        VkBuffer dst_buffer, VkDeviceSize dst_offset, VkDeviceSize size) {
        submitImmediately([src_buffer, src_offset, dst_buffer, dst_offset, size](VkCommandBuffer cmd) {
            VkBufferCopy copy_region;
            copy_region.srcOffset = src_offset;
            copy_region.dstOffset = dst_offset;
            copy_region.size = size;
            vkCmdCopyBuffer(cmd, src_buffer, dst_buffer, 1, &copy_region);
        });
    }

    void Device::copyBufferToImage(VkBuffer buffer, VkDeviceSize buffer_offset, VkImage image, u32 width, u32 height) {
        submitImmediately([buffer, buffer_offset, image, width, height](VkCommandBuffer cmd) {
            VkBufferImageCopy region;
            region.bufferOffset = buffer_offset;
            region.bufferRowLength = 0;
            region.bufferImageHeight = 0;
            region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            region.imageSubresource.mipLevel = 0;
            region.imageSubresource.baseArrayLayer = 0;
            region.imageSubresource.layerCount = 1;
            region.imageOffset = { 0, 0, 0 };
            region.imageExtent = { width, height, 1 };

            vkCmdCopyBufferToImage(cmd, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
            });
    }

    void Device::transitionImageLayout(VkImage image, VkFormat format, VkImageLayout old_layout,
	    VkImageLayout new_layout) {
        submitImmediately([image, format, old_layout, new_layout](VkCommandBuffer cmd) {
            VkImageMemoryBarrier barrier{ VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER };
            barrier.oldLayout = old_layout;
            barrier.newLayout = new_layout;
            barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.image = image;
            barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            barrier.subresourceRange.baseMipLevel = 0;
            barrier.subresourceRange.levelCount = 1;
            barrier.subresourceRange.baseArrayLayer = 0;
            barrier.subresourceRange.layerCount = 1;

            VkPipelineStageFlags src_stage;
            VkPipelineStageFlags dst_stage;

            if (old_layout == VK_IMAGE_LAYOUT_UNDEFINED
                && new_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
                barrier.srcAccessMask = 0;
                barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
                src_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
                dst_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
            }
            else if (old_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
                && new_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
                barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
                barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
                src_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
                dst_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
            }
            else {
                throw std::invalid_argument("unsupported layout transition!");
            }


            vkCmdPipelineBarrier(cmd, src_stage, dst_stage, 0, 0, nullptr,
                0, nullptr, 1, &barrier);
            });
    }

    void Device::submitImmediately(const std::function<void(VkCommandBuffer)>& fn) {
        VkCommandBufferAllocateInfo cmd_buffer_info{ VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO };
        cmd_buffer_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        cmd_buffer_info.commandPool = m_command_pool;
        cmd_buffer_info.commandBufferCount = 1;

        VkCommandBuffer command_buffer;// TODO: Assert
        vkAllocateCommandBuffers(m_device, &cmd_buffer_info, &command_buffer);

        VkCommandBufferBeginInfo begin_info{ VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
        begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        vkBeginCommandBuffer(command_buffer, &begin_info);

        fn(command_buffer);

        vkEndCommandBuffer(command_buffer);

        VkSubmitInfo submit_info{ VK_STRUCTURE_TYPE_SUBMIT_INFO };
        submit_info.commandBufferCount = 1;
        submit_info.pCommandBuffers = &command_buffer;

        vkQueueSubmit(m_graphics_queue, 1, &submit_info, VK_NULL_HANDLE);
        vkQueueWaitIdle(m_graphics_queue);
        vkFreeCommandBuffers(m_device, m_command_pool, 1, &command_buffer);
    }

    void Device::beginFrame()
    {
        m_frame_in_flight = m_num_frames++ % m_num_frames_in_flight;
        vkWaitForFences(m_device, 1, &m_in_flight_fence[m_frame_in_flight], VK_TRUE, UINT64_MAX);
        vkResetFences(m_device, 1, &m_in_flight_fence[m_frame_in_flight]);

        m_swap_chain->acquireNextImage(m_image_available_semaphore[m_frame_in_flight]);

        vkResetCommandBuffer(getFrameCommandBuffer(), 0);
        beginRecordingCommandBuffer(getFrameCommandBuffer());

        const VkImageMemoryBarrier image_memory_barrier{
		    .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
		    .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
		    .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
		    .newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
		    .image = m_swap_chain->getCurrentImage(),
		    .subresourceRange = {
		      .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
		      .baseMipLevel = 0,
		      .levelCount = 1,
		      .baseArrayLayer = 0,
		      .layerCount = 1,
		    }
        };

        vkCmdPipelineBarrier(
            getFrameCommandBuffer(),
            VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,  // srcStageMask
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // dstStageMask
            0,
            0,
            nullptr,
            0,
            nullptr,
            1, // imageMemoryBarrierCount
            &image_memory_barrier // pImageMemoryBarriers
        );


        VkRenderingAttachmentInfoKHR color_attachment_info{ VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO_KHR };
        color_attachment_info.imageView = m_swap_chain->getCurrentImageView();
        color_attachment_info.imageLayout = VK_IMAGE_LAYOUT_ATTACHMENT_OPTIMAL;
        color_attachment_info.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        color_attachment_info.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        color_attachment_info.clearValue = { {{0.0f, 0.0f, 0.0f, 1.0f}} };

        VkRenderingInfoKHR render_info{ VK_STRUCTURE_TYPE_RENDERING_INFO_KHR };
        render_info.renderArea.offset = { 0, 0 };
        render_info.renderArea.extent = m_swap_chain->getExtent();
        render_info.colorAttachmentCount = 1;
        render_info.pColorAttachments = &color_attachment_info;
        render_info.layerCount = 1;
        render_info.pDepthAttachment = nullptr;
        render_info.pStencilAttachment = nullptr;
        vkCmdBeginRenderingKHR(getFrameCommandBuffer(), &render_info);

        vkCmdBindPipeline(getFrameCommandBuffer(), VK_PIPELINE_BIND_POINT_GRAPHICS, m_graphics_pipeline->getPipeline());

        vkCmdBindDescriptorSets(getFrameCommandBuffer(), VK_PIPELINE_BIND_POINT_GRAPHICS, m_graphics_pipeline->getLayout(),
            0, 1, &m_descriptor_sets[m_frame_in_flight], 0, nullptr);
    }

    void Device::endFrame()
    {
        vkCmdEndRenderingKHR(getFrameCommandBuffer());

        const VkImageMemoryBarrier image_memory_barrier{
		    .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
		    .srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
		    .oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
		    .newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
		    .image = m_swap_chain->getCurrentImage(),
		    .subresourceRange = {
		        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
		        .baseMipLevel = 0,
		        .levelCount = 1,
		        .baseArrayLayer = 0,
		        .layerCount = 1
		    }
        };

        vkCmdPipelineBarrier(
            getFrameCommandBuffer(),
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
            0,
            0,
            nullptr,
            0,
            nullptr,
            1,
            &image_memory_barrier
        );

        endRecordingCommandBuffer(getFrameCommandBuffer());
        VkSubmitInfo submit_info{ VK_STRUCTURE_TYPE_SUBMIT_INFO };

        VkSemaphore wait_semaphores[] = { m_image_available_semaphore[m_frame_in_flight]};
        VkPipelineStageFlags wait_stages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
        submit_info.waitSemaphoreCount = 1;
        submit_info.pWaitSemaphores = wait_semaphores;
        submit_info.pWaitDstStageMask = wait_stages;
        submit_info.commandBufferCount = 1;
        submit_info.pCommandBuffers = &m_frame_command_buffers[m_frame_in_flight];

        VkSemaphore signal_semaphores[] = { m_render_finished_semaphore[m_frame_in_flight]};
        submit_info.signalSemaphoreCount = 1;
        submit_info.pSignalSemaphores = signal_semaphores;

        if (vkQueueSubmit(m_graphics_queue, 1, &submit_info,
            m_in_flight_fence[m_frame_in_flight]) != VK_SUCCESS) {
            throw std::runtime_error("Failed to submit draw command buffer");
        }

        VkPresentInfoKHR present_info{ VK_STRUCTURE_TYPE_PRESENT_INFO_KHR };
        present_info.waitSemaphoreCount = 1;
        present_info.pWaitSemaphores = signal_semaphores;

        VkSwapchainKHR swapChains[] = { m_swap_chain->getSwapChain() };
        present_info.swapchainCount = 1;
        present_info.pSwapchains = swapChains;
        present_info.pImageIndices = &m_swapchain_image_index;
        present_info.pResults = nullptr;

        vkQueuePresentKHR(m_present_queue, &present_info);
    }
}
