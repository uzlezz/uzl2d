#include "renderer/gpu_buffer.h"

#include "renderer/renderer.h"

namespace uz
{
	std::shared_ptr<GpuBuffer> GpuBuffer::create(Device& device, const GpuBufferSpecification& spec) {
		auto usage = spec.usage;
		if (spec.device_address)
			usage |= VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;

		VkBufferCreateInfo buffer_info = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
		buffer_info.size = spec.size;
		buffer_info.pNext = nullptr;
		buffer_info.usage = usage;

		VmaAllocationCreateInfo alloc_info{};
		alloc_info.usage = spec.mem_usage;
		alloc_info.flags = spec.mapped ? VMA_ALLOCATION_CREATE_MAPPED_BIT : 0;

		auto buffer = std::make_shared<GpuBuffer>();
		buffer->m_device = &device;

		// TODO: Assert
		vmaCreateBuffer(device.getAllocator(), &buffer_info, &alloc_info, &buffer->m_buffer,
			&buffer->m_allocation, &buffer->m_allocation_info);

		if (spec.device_address) {
			VkBufferDeviceAddressInfo device_address_info{ VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO };
			device_address_info.buffer = buffer->m_buffer;
			buffer->m_device_address = vkGetBufferDeviceAddress(device.getDevice(), &device_address_info);
		}

		return buffer;
	}

	GpuBuffer::~GpuBuffer() {
		vmaDestroyBuffer(m_device->getAllocator(), m_buffer, m_allocation);
	}
}
