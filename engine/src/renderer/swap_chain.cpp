#include "renderer/swap_chain.h"

#include <algorithm>
#include <optional>
#include <stdexcept>

#include "GLFW/glfw3.h"
#include "renderer/renderer.h"

namespace uz
{

        SwapChainSupportDetails SwapChain::querySwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR surface) {
            SwapChainSupportDetails details;

            vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);

            u32 formatCount;
            vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);

            if (formatCount != 0) {
                details.formats.resize(formatCount);
                vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.formats.data());
            }

            u32 presentModeCount;
            vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);

            if (presentModeCount != 0) {
                details.present_modes.resize(presentModeCount);
                vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.present_modes.data());
            }

            return details;
        }

        VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& available_formats) {
            for (const auto& format : available_formats) {
                if (format.format == VK_FORMAT_B8G8R8A8_SRGB && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
                    return format;
                }
            }

            // TODO: rate formats by suitability
            return available_formats[0];
        }

        VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& available_modes) {
            for (auto mode : available_modes) {
                if (mode == VK_PRESENT_MODE_MAILBOX_KHR) {
                    // Triple buffering, better for performance
                    // Not guaranteed to exist
                    return mode;
                }
            }

            // Always guaranteed to exist
            return VK_PRESENT_MODE_FIFO_KHR;
        }


        VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities, GLFWwindow* window) {
            if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
                return capabilities.currentExtent;
            }
            else {
                int width, height;
                glfwGetFramebufferSize(window, &width, &height);

                VkExtent2D actual_extent = {
                        static_cast<uint32_t>(width),
                        static_cast<uint32_t>(height)
                };

                actual_extent.width = std::clamp(actual_extent.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
                actual_extent.height = std::clamp(actual_extent.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);

                return actual_extent;
            }
        }

	SwapChain::SwapChain(Device& device)
		: m_device(&device) {
        if (m_swap_chain != VK_NULL_HANDLE) {
            for (auto& view : m_swap_chain_image_views) {
                vkDestroyImageView(m_device->getDevice(), view, nullptr);
            }

            vkDestroySwapchainKHR(m_device->getDevice(), m_swap_chain, nullptr);
        }

        auto support_details = querySwapChainSupport(m_device->getPhysicalDevice(), m_device->getSurface());

        auto surface_format = chooseSwapSurfaceFormat(support_details.formats);
        auto present_mode = chooseSwapPresentMode(support_details.present_modes);
        auto extent = chooseSwapExtent(support_details.capabilities, m_device->window());

        u32 image_count = support_details.capabilities.minImageCount + 1;
        if (support_details.capabilities.maxImageCount > 0 && image_count > support_details.capabilities.maxImageCount)
            image_count = support_details.capabilities.maxImageCount;

        VkSwapchainCreateInfoKHR swap_chain_info{ VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR };
        swap_chain_info.surface = m_device->getSurface();
        swap_chain_info.minImageCount = image_count;
        swap_chain_info.imageColorSpace = surface_format.colorSpace;
        swap_chain_info.imageFormat = surface_format.format;
        swap_chain_info.imageExtent = extent;
        swap_chain_info.imageArrayLayers = 1;
        swap_chain_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

        auto indices = Device::findQueueFamilies(m_device->getPhysicalDevice(), m_device->getSurface());
        u32 queue_family_indices[] = { indices.graphics_family.value(), indices.present_family.value() };

        if (indices.graphics_family != indices.present_family) {
            swap_chain_info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
            swap_chain_info.queueFamilyIndexCount = 2;
            swap_chain_info.pQueueFamilyIndices = queue_family_indices;
        }
        else {
            swap_chain_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
            swap_chain_info.queueFamilyIndexCount = 0; // Optional
            swap_chain_info.pQueueFamilyIndices = nullptr; // Optional
        }

        swap_chain_info.preTransform = support_details.capabilities.currentTransform;
        swap_chain_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        swap_chain_info.presentMode = present_mode;
        swap_chain_info.clipped = VK_TRUE;
        swap_chain_info.oldSwapchain = VK_NULL_HANDLE;

        if (vkCreateSwapchainKHR(m_device->getDevice(), &swap_chain_info, nullptr, &m_swap_chain) != VK_SUCCESS) {
            throw std::runtime_error("Cannot create swap chain");
        }

        vkGetSwapchainImagesKHR(m_device->getDevice(), m_swap_chain, &image_count, nullptr);
        m_swap_chain_images.clear();
        m_swap_chain_images.resize(image_count);
        vkGetSwapchainImagesKHR(m_device->getDevice(), m_swap_chain, &image_count, m_swap_chain_images.data());

        m_swap_chain_image_format = surface_format.format;
        m_swap_chain_extent = extent;

        m_swap_chain_image_views.resize(m_swap_chain_images.size());
        for (u64 i = 0; i < m_swap_chain_images.size(); ++i) {
            VkImageViewCreateInfo view_info{ VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO };
            view_info.image = m_swap_chain_images[i];
            view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
            view_info.format = m_swap_chain_image_format;
            view_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
            view_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
            view_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
            view_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
            view_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            view_info.subresourceRange.baseMipLevel = 0;
            view_info.subresourceRange.levelCount = 1;
            view_info.subresourceRange.baseArrayLayer = 0;
            view_info.subresourceRange.layerCount = 1;

            if (vkCreateImageView(m_device->getDevice(), &view_info, nullptr, &m_swap_chain_image_views[i])) {
                throw std::runtime_error("Cannot create image view for swap chain image");
            }
        }
	}

	SwapChain::~SwapChain() {
        for (auto& view : m_swap_chain_image_views) {
            vkDestroyImageView(m_device->getDevice(), view, nullptr);
        }
        vkDestroySwapchainKHR(m_device->getDevice(), m_swap_chain, nullptr);
	}

	void SwapChain::acquireNextImage(VkSemaphore semaphore) {
        vkAcquireNextImageKHR(m_device->getDevice(), m_swap_chain, UINT64_MAX,
            semaphore, VK_NULL_HANDLE, &m_image_index);
	}
}
