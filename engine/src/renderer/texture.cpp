#include "renderer/texture.h"
#include "renderer/renderer.h"

#include <iostream>
#include <stdexcept>

#include "fs/virtual_file_system.h"
#include <stb/stb_image.h>

namespace uz {

    std::shared_ptr<Texture2D> Texture2D::create(Device& device, std::string_view path) {
        auto file = vfs->openFile({ path }, IFile::OpenMode::In);
        if (!file->isOpened())
            throw std::runtime_error("Cannot open file to load texture from");

        std::vector<u8> data;
        data.resize(file->getSize());
        const auto read = file->read(data.data(), data.size());
        if (read != data.size())
            throw std::runtime_error("Could not read entire texture into a buffer");
        file->close();

        int width, height, num_channels;
        stbi_info_from_memory(data.data(), static_cast<int>(data.size()), &width, &height, &num_channels);
        auto pixels = stbi_load_from_memory(data.data(), static_cast<int>(data.size()),
            &width, &height, &num_channels, STBI_rgb_alpha);

        if (!pixels)
            throw std::runtime_error("Could not load pixels from a texture");

        if (num_channels == 3)
        {
            const auto pixels3 = pixels;
            pixels = static_cast<stbi_uc*>(malloc(width * height * 4));

            for (int i = 0, j = 0; i < width * height * 3; i += 3, j += 4)
            {
                pixels[j]     = pixels3[i];
                pixels[j + 1] = pixels3[i + 1];
                pixels[j + 2] = pixels3[i + 2];
                pixels[j + 3] = 255;
            }

            stbi_image_free(pixels3);
            num_channels = 4;
        }

        TextureSpecification spec;
        spec.width = static_cast<u32>(width);
        spec.height = static_cast<u32>(height);
        spec.data = pixels;
        auto image = create(device, spec);

        free(pixels);
        return image;
    }

    std::shared_ptr<Texture2D> Texture2D::create(Device& device, const TextureSpecification& spec) {
        VkDeviceSize image_size = spec.width * spec.height * spec.pixel_size;

        VkBuffer staging_buffer;
        VmaAllocation allocation;
        device.createStagingBuffer(image_size, staging_buffer, allocation);

        vmaCopyMemoryToAllocation(device.getAllocator(), spec.data, allocation, 0, image_size);

        VkFormat format = spec.format;

        VkImageCreateInfo image_info{ VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO };
        image_info.imageType = VK_IMAGE_TYPE_2D;
        image_info.extent.width = spec.width;
        image_info.extent.height = spec.height;
        image_info.extent.depth = 1;
        image_info.mipLevels = 1;
        image_info.arrayLayers = 1;
        image_info.format = format;
        image_info.tiling = VK_IMAGE_TILING_OPTIMAL;
        image_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        image_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        image_info.samples = VK_SAMPLE_COUNT_1_BIT;
        image_info.flags = 0;
        image_info.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;

        VmaAllocationCreateInfo image_alloc_info = {};
        image_alloc_info.usage = VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE;
        image_alloc_info.flags = VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT;
        image_alloc_info.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
        image_alloc_info.priority = 1.0f;

        auto image = std::make_shared<Texture2D>();
        image->m_device = &device;
        image->m_format = format;
        // TODO: Assert
        vmaCreateImage(device.getAllocator(), &image_info, &image_alloc_info, &image->m_image,
            &image->m_allocation, nullptr);

        device.transitionImageLayout(image->m_image, image->m_format,
            VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
        device.copyBufferToImage(staging_buffer, 0, image->m_image,
            spec.width, spec.height);
        device.transitionImageLayout(image->m_image, image->m_format,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

        vmaDestroyBuffer(device.getAllocator(), staging_buffer, allocation);



        VkImageViewCreateInfo view_info{ VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO };
        view_info.image = image->m_image;
        view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
        view_info.format = image->m_format;
        view_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        view_info.subresourceRange.baseMipLevel = 0;
        view_info.subresourceRange.levelCount = 1;
        view_info.subresourceRange.baseArrayLayer = 0;
        view_info.subresourceRange.layerCount = 1;

        // TODO: Assert
        vkCreateImageView(device.getDevice(), &view_info, nullptr, &image->m_view);



        VkSamplerCreateInfo sampler_info{ VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO };
        sampler_info.magFilter = spec.filter;
        sampler_info.minFilter = spec.filter;
        sampler_info.addressModeU = spec.address_mode;
        sampler_info.addressModeV = spec.address_mode;
        sampler_info.addressModeW = spec.address_mode;
        sampler_info.anisotropyEnable = VK_FALSE;
        sampler_info.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
        sampler_info.unnormalizedCoordinates = VK_FALSE;
        sampler_info.compareEnable = VK_FALSE;
        sampler_info.compareOp = VK_COMPARE_OP_ALWAYS;
        sampler_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
        sampler_info.mipLodBias = 0.0f;
        sampler_info.minLod = 0.0f;
        sampler_info.maxLod = 0.0f;

        // TODO: Assert
        vkCreateSampler(device.getDevice(), &sampler_info, nullptr, &image->m_sampler);



        if (spec.bindless_register)
            image->m_handle = device.getTexturesManager().registerTexture(image->getView(), image->getSampler());

        return image;
    }

    Texture2D::~Texture2D() {
        if (m_handle != 0)
            m_device->getTexturesManager().unregisterTexture(m_handle);

        vkDestroySampler(m_device->getDevice(), m_sampler, nullptr);
        vkDestroyImageView(m_device->getDevice(), m_view, nullptr);
        vmaDestroyImage(m_device->getAllocator(), m_image, m_allocation);
    }



    BindlessTexturesManager::BindlessTexturesManager(Device& device, u32 max_textures)
        : m_device(&device), m_max_textures(max_textures) {
        VkDescriptorPoolSize pool_size;
        pool_size.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        pool_size.descriptorCount = m_max_textures;

        VkDescriptorPoolCreateInfo pool_info{ VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO };
        pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT;
        pool_info.maxSets = m_max_textures;
        pool_info.poolSizeCount = 1;
        pool_info.pPoolSizes = &pool_size;

        // TODO: Assert
        vkCreateDescriptorPool(m_device->getDevice(), &pool_info, nullptr, &m_descriptor_pool);
        m_slots.resize(m_max_textures);

        constexpr VkDescriptorBindingFlags bindless_flags =
            VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT
            | VK_DESCRIPTOR_BINDING_VARIABLE_DESCRIPTOR_COUNT_BIT
            | VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT;

        VkDescriptorSetLayoutBinding binding;
        binding.binding = bindless_texture_binding;
        binding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        binding.descriptorCount = m_max_textures;
        binding.stageFlags = VK_SHADER_STAGE_ALL_GRAPHICS;
        binding.pImmutableSamplers = nullptr;

        VkDescriptorSetLayoutCreateInfo layout_info{ VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO };
        layout_info.flags = VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT;
        layout_info.bindingCount = 1;
        layout_info.pBindings = &binding;

        VkDescriptorSetLayoutBindingFlagsCreateInfo extended_layout_info
        { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO };
        extended_layout_info.bindingCount = 1;
        extended_layout_info.pBindingFlags = &bindless_flags;

        layout_info.pNext = &extended_layout_info;

        // TODO: Assert
        vkCreateDescriptorSetLayout(m_device->getDevice(), &layout_info, nullptr, &m_descriptor_set_layout);

        VkPipelineLayoutCreateInfo pipeline_layout_create_info{ VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO };
        pipeline_layout_create_info.setLayoutCount = 1;
        pipeline_layout_create_info.pSetLayouts = &m_descriptor_set_layout;

        // TODO: Assert
        vkCreatePipelineLayout(m_device->getDevice(), &pipeline_layout_create_info, nullptr, &m_pipeline_layout);

        VkDescriptorSetAllocateInfo alloc_info{ VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO };
        alloc_info.descriptorPool = m_descriptor_pool;
        alloc_info.descriptorSetCount = 1;
        alloc_info.pSetLayouts = &m_descriptor_set_layout;

        const u32 max_bindings = m_max_textures - 1;
        VkDescriptorSetVariableDescriptorCountAllocateInfo count_info
    	{ VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_ALLOCATE_INFO };
        count_info.descriptorSetCount = 1;
        count_info.pDescriptorCounts = &max_bindings;

        alloc_info.pNext = &count_info;

        // TODO: Assert
        vkAllocateDescriptorSets(m_device->getDevice(), &alloc_info, &m_descriptor_set);

        constexpr u32 p = 0xFFEE00FF;
        constexpr u32 b = 0xFF000000;
        // clang-format off
        auto data =
        {
            p, b, p, b, p, b, p, b,
            b, p, b, p, b, p, b, p,
            p, b, p, b, p, b, p, b,
            b, p, b, p, b, p, b, p,
            p, b, p, b, p, b, p, b,
            b, p, b, p, b, p, b, p,
            p, b, p, b, p, b, p, b,
            b, p, b, p, b, p, b, p
        };
        // clang-format on

        TextureSpecification spec;
        spec.width = spec.height = 8;
        spec.data = static_cast<const void*>(data.begin());
        spec.bindless_register = false;
        m_error_texture = Texture2D::create(*m_device, spec);

        VkDescriptorImageInfo image_info{};
        image_info.imageView = m_error_texture->getView();
        image_info.sampler = m_error_texture->getSampler();
        image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        std::vector<VkWriteDescriptorSet> writes(max_bindings);
        for (u32 i = 0; i < writes.size(); ++i) {

            VkWriteDescriptorSet write{ VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET };
            write.dstSet = m_descriptor_set;
            write.dstBinding = bindless_texture_binding;
            write.dstArrayElement = i;
            write.descriptorCount = 1;
            write.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            write.pImageInfo = &image_info;

            writes[i] = write;
        }
        vkUpdateDescriptorSets(m_device->getDevice(), static_cast<u32>(writes.size()),
            writes.data(), 0, nullptr);
    }

    BindlessTexturesManager::~BindlessTexturesManager() {
        m_error_texture.reset();
        vkDestroyPipelineLayout(m_device->getDevice(), m_pipeline_layout, nullptr);
        vkDestroyDescriptorSetLayout(m_device->getDevice(), m_descriptor_set_layout, nullptr);
        vkDestroyDescriptorPool(m_device->getDevice(), m_descriptor_pool, nullptr);
    }

    u32 BindlessTexturesManager::registerTexture(VkImageView image_view, VkSampler sampler) {
        u32 handle = 0;
        while (m_slots[handle]) ++handle;
        m_slots[handle] = true;

        VkDescriptorImageInfo image_info{};
        image_info.sampler = sampler;
        image_info.imageView = image_view;
        image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        VkWriteDescriptorSet write{ VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET };
        write.dstSet = m_descriptor_set;
        write.dstBinding = bindless_texture_binding;
        write.dstArrayElement = handle;
        write.descriptorCount = 1;
        write.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        write.pImageInfo = &image_info;

        vkUpdateDescriptorSets(m_device->getDevice(), 1, &write, 0, nullptr);
        return handle;
    }

    void BindlessTexturesManager::unregisterTexture(u32 handle) {
        m_slots[handle] = false;
        if (!m_error_texture) return;

        VkDescriptorImageInfo image_info{};
        image_info.sampler = m_error_texture->getSampler();
        image_info.imageView = m_error_texture->getView();
        image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        VkWriteDescriptorSet write{ VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET };
        write.dstSet = m_descriptor_set;
        write.dstBinding = bindless_texture_binding;
        write.dstArrayElement = handle;
        write.descriptorCount = 1;
        write.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        write.pImageInfo = &image_info;

        vkUpdateDescriptorSets(m_device->getDevice(), 1, &write, 0, nullptr);
    }
}
