#include "utils.h"
#include "fs/virtual_file_system.h"
#include <fstream>
#include <filesystem>

namespace uz
{
    VirtualFileSystem* vfs = nullptr;
}

namespace uz::utils
{

    void initVFS() {
        uz::vfs = new uz::VirtualFileSystem();
    }

    bool loadFileFromData(std::string_view file_name, std::vector<u8>& data) {
        auto f = vfs->openFile({ file_name }, IFile::In);
        if (!f || !f->isOpened()) return false;

        data.resize(f->getSize());
        f->read(data.data(), f->getSize());
        return true;
    }

}