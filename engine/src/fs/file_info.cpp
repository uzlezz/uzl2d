#include "fs/file_info.h"

namespace uz {

    FileInfo::FileInfo(std::string_view path) : m_is_directory(false) {
        const auto found = path.rfind('/');
        if (found == std::string_view::npos) return;

        FixedStr<255> fpath(path);
        auto base_path = FixedStr<191>(fpath.substr(0, found + 1));
        base_path.resize(found + 1);
        std::string_view name;
        if (found != path.length())
            name = path.substr(found + 1, path.length() - found - 1);

        init(static_cast<std::string_view>(base_path), name, false);
    }

    FileInfo::FileInfo(std::string_view base_path, std::string_view name, bool is_directory) {
        if (name.starts_with('/'))
            name = name.substr(1);

        const FixedStr<64> fname(name);
        const auto found = fname.rfind('/');
        if (found == fname.npos) {
            init(base_path, name, is_directory);
            return;
        }

        std::string bp = base_path.data();
        if (!base_path.ends_with('/')) bp += '/';
        bp += name.substr(0, found + 1);
        init(bp, name.substr(found + 1, name.length() - found - 1), is_directory);
    }

    void FileInfo::init(std::string_view base_path, std::string_view name, bool is_directory) {
        m_base_path = base_path;
        m_name = name;
        m_is_directory = is_directory;

        if (!m_base_path.endsWith('/'))
            m_base_path += '/';

        if (is_directory && !m_name.endsWith('/'))
            m_name += '/';

        if (m_name.startsWith('/'))
            m_name = m_name.substr(1, m_name.length() - 1);

        m_absolute_path = m_base_path + m_name;

        if (!is_directory && std::count(m_name.begin(), m_name.end(), '.') != m_name.length()) {
            const auto found = m_name.rfind('.');
            if (found != FixedStr<64>::npos) {
                m_base_name = m_name.substr(0, found);
                if (found < m_name.length())
                    m_extension_offset = found;
            }
        }
    }

    std::string_view FileInfo::getExtension() const {
        return m_extension_offset != 255 ? std::string_view(m_absolute_path.data() + m_extension_offset) : "";
    }

}