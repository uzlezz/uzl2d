#include "fs/virtual_file_system.h"

#include <algorithm>

namespace uz {

    static std::string normalizeAlias(std::string_view alias) {
        std::string a = alias.data();
        if (!alias.ends_with('/')) a += '/';
        return a;
    }

    VirtualFileSystem::~VirtualFileSystem() {
        for (auto &fs: m_file_system) {
            fs.second->shutdown();
        }
    }

    void VirtualFileSystem::mount(std::string_view alias_, const IFileSystem::Ptr& fs) {
        if (!fs) return;

        const auto alias = normalizeAlias(alias_);
        const auto it = m_file_system.find(alias);
        if (it != m_file_system.end()) return;

        m_file_system[alias] = fs;
        m_descriptors.emplace_back(alias, fs);
        m_descriptors.sort([](const FileSystemDescriptor &lhs, const FileSystemDescriptor &rhs) {
            return lhs.alias.length() > rhs.alias.length();
        });
    }

    void VirtualFileSystem::unmount(std::string_view alias_) {
        const auto alias = normalizeAlias(alias_);

        const auto it = m_file_system.find(alias);
        if (it != m_file_system.end()) {
            std::erase_if(m_descriptors, [&it](const FileSystemDescriptor &desc) {
                return desc.alias == it->first;
            });
            m_file_system.erase(it);
        }
    }

    bool VirtualFileSystem::isFileSystemExists(std::string_view alias) const {
        return m_file_system.contains(normalizeAlias(alias));
    }

    IFileSystem::Ptr VirtualFileSystem::getFileSystem(std::string_view alias) {
        return m_file_system.find(normalizeAlias(alias))->second;
    }

    IFile::Ptr VirtualFileSystem::openFile(const FileInfo &info, IFile::OpenMode mode) {
        IFile::Ptr file = nullptr;

        for (const auto &desc: m_descriptors) {
            if (info.getParentDirectory().starts_with(desc.alias) &&
                info.getAbsolutePath().length() != desc.alias.length()) {
                FileInfo file_info(info.getParentDirectory().substr(desc.alias.size()), info.getName(), false);
                file = desc.file_system->openFile(file_info, mode);

                if (file) {
                    const auto addr = reinterpret_cast<uintptr_t>(static_cast<void *>(file.get()));
                    m_opened_files[addr] = desc.file_system;
                    break;
                }
            }
        }

        return file;
    }

    void VirtualFileSystem::closeFile(IFile::Ptr file) {
        const auto addr = reinterpret_cast<uintptr_t>(static_cast<void *>(file.get()));

        const auto it = m_opened_files.find(addr);
        if (it != m_opened_files.end()) {
            it->second->closeFile(file);
            m_opened_files.erase(it);
        }
    }

}