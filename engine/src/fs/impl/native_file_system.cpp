#include "fs/impl/native_file_system.h"
#include "fs/impl/native_file.h"
#include <filesystem>
#include <valarray>

namespace uz {

    NativeFileSystem::NativeFileSystem(std::string_view path)
            : m_base_path(path), m_is_initialized(false) {
        if (!m_base_path.ends_with('/')) m_base_path += '/';
        init();
    }

    NativeFileSystem::~NativeFileSystem() {
    }

    void NativeFileSystem::init() {
        if (m_is_initialized) return;

        if (std::filesystem::exists(m_base_path) && std::filesystem::is_directory(m_base_path)) {
            buildFileList();
            m_is_initialized = true;
        }
    }

    void NativeFileSystem::shutdown() {
        m_base_path = "";
        m_file_list.clear();
        m_is_initialized = false;
    }

    bool NativeFileSystem::isReadOnly() const {
        return !m_is_initialized || (std::filesystem::perms::owner_read
                                     & std::filesystem::status(m_base_path).permissions()) ==
                                    std::filesystem::perms::none;
    }

    IFile::Ptr NativeFileSystem::openFile(const FileInfo &info, IFile::OpenMode mode) {
        FixedStr<255> path(m_base_path + (info.getAbsolutePath().data() + 1));
        FileInfo file_info(path.data());
        auto file = findFile(file_info);

        bool exists = file != nullptr;
        if (!exists) {
            mode = static_cast<IFile::OpenMode>(static_cast<int>(mode) & IFile::Truncate);
            file.reset(new NativeFile(file_info));
        }
        file->open(mode);

        if (!exists && file->isOpened()) {
            m_file_list.insert(file);
        }
        return file;
    }

    void NativeFileSystem::closeFile(IFile::Ptr file) {
        if (file) file->close();
    }

    bool NativeFileSystem::createFile(const FileInfo &info) {
        if (isReadOnly()) return false;

        if (!exists(info)) {
            auto file = openFile(info, static_cast<IFile::OpenMode>(IFile::Out | IFile::Truncate));
            if (file) {
                file->close();
                return true;
            }
            return false;
        }

        return true;
    }

    bool NativeFileSystem::removeFile(const FileInfo &info) {
        if (isReadOnly()) return false;

        auto file = findFile(info);
        if (file) {
            FileInfo file_info(getBasePath(), file->getFileInfo().getAbsolutePath(), false);
            if (std::filesystem::remove(file_info.getAbsolutePath())) {
                m_file_list.erase(file);
                return true;
            }
        }

        return false;
    }

    static constexpr u64 chunk_size = 1024;

    bool NativeFileSystem::copyFile(const FileInfo &src, const FileInfo &dst) {
        if (isReadOnly()) return false;

        auto from = findFile(src);
        auto to = openFile(dst, IFile::Out);
        if (!from || !to) return false;

        u64 size = chunk_size;
        std::vector<u8> buff(size);
        do {
            size = from->read(buff.data(), chunk_size);
            to->write(buff.data(), size);
        } while (size == chunk_size);
        return true;
    }

    bool NativeFileSystem::renameFile(const FileInfo &src, const FileInfo &dst) {
        if (isReadOnly()) return false;

        auto from = findFile(src);
        auto to = findFile(dst);
        if (from && to) return false;

        FileInfo to_info(getBasePath(), dst.getAbsolutePath(), false);
        std::error_code ec;
        std::filesystem::rename(from->getFileInfo().getAbsolutePath(),
                                to_info.getAbsolutePath(), ec);

        if (!ec) {
            m_file_list.erase(from);
            to = openFile(dst, IFile::In);
            if (to) {
                to->close();
                return true;
            }
        }

        return false;
    }

    bool NativeFileSystem::exists(const FileInfo &info) const {
        return std::filesystem::exists(m_base_path + info.getAbsolutePath().data());
    }

    bool NativeFileSystem::isFile(const FileInfo &info) const {
        auto file = findFile(info);
        if (file) {
            return !file->getFileInfo().isDirectory();
        }
        return false;
    }

    bool NativeFileSystem::isDirectory(const FileInfo &info) const {
        return !isFile(info);
    }

    IFile::Ptr NativeFileSystem::findFile(const FileInfo &info) const {
        IFile::Ptr file = nullptr;
        for (const auto& f : m_file_list) {
            if (std::strcmp(f->getFileInfo().getAbsolutePath().data(), info.getAbsolutePath().data()) == 0) {
                file = f;
                break;
            }
        }

        return file;
    }

    void NativeFileSystem::buildFileList() {
        if (!m_base_path.ends_with('/'))
            m_base_path += '/';

        for (const auto &entry: std::filesystem::recursive_directory_iterator(m_base_path)) {
            auto filename = entry.path().filename().generic_string();
            if (filename == "." || filename == "..") continue;

            const auto& abs_path = entry.path();

            FileInfo file_info(m_base_path, relative(abs_path, m_base_path)
                    .generic_string(), false);
            if (!findFile(file_info)) {
                IFile::Ptr file(new NativeFile(file_info));
                m_file_list.insert(file);
            }
        }
    }

}