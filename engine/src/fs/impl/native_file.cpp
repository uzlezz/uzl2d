#include "fs/impl/native_file.h"

namespace uz {

    NativeFile::NativeFile(const FileInfo &info)
            : m_info(info), m_is_read_only(true), m_mode(None) {}

    NativeFile::~NativeFile() {
        close();
    }

    u64 NativeFile::getSize() {
        if (isOpened()) {
            auto current = tell();
            seek(0, SeekOrigin::End);
            const auto size = tell();
            seek(current, SeekOrigin::Begin);

            return size;
        }

        return 0;
    }

    void NativeFile::open(OpenMode mode) {
        if (isOpened() && m_mode == mode) {
            seek(0, SeekOrigin::Begin);
            return;
        }

        m_mode = mode;
        m_is_read_only = true;

        auto open_mode = std::fstream::binary;
        if (mode & OpenMode::In)
            open_mode |= std::fstream::in;

        if (mode & OpenMode::Out) {
            m_is_read_only = false;
            open_mode |= std::fstream::out;
        }

        if (mode & OpenMode::Append) {
            m_is_read_only = false;
            open_mode |= std::fstream::app;
        }

        if (mode & OpenMode::Truncate)
            open_mode |= std::fstream::trunc;

        m_stream.open(getFileInfo().getAbsolutePath().data(), open_mode);
    }

    void NativeFile::close() {
        m_stream.close();
    }

    bool NativeFile::isOpened() const {
        return m_stream.is_open();
    }

    u64 NativeFile::seek(u64 offset, SeekOrigin origin) {
        if (!isOpened()) return 0;

        std::ios_base::seekdir way;
        if (origin == SeekOrigin::Begin)
            way = std::ios_base::beg;
        else if (origin == SeekOrigin::End)
            way = std::ios_base::end;
        else
            way = std::ios_base::cur;

        m_stream.seekg(static_cast<off_t>(offset), way);
        m_stream.seekp(static_cast<off_t>(offset), way);

        return tell();
    }

    u64 NativeFile::tell() {
        return m_stream.tellg();
    }

    u64 NativeFile::read(u8 *buffer, u64 size) {
        if (!isOpened()) return 0;

        m_stream.read(reinterpret_cast<char *>(buffer), static_cast<std::streamsize>(size));
        if (m_stream)
            return size;

        return static_cast<u64>(m_stream.gcount());
    }

    u64 NativeFile::write(const u8 *buffer, u64 size) {
        if (!isOpened() || isReadOnly()) return 0;

        m_stream.write(reinterpret_cast<const char *>(buffer), static_cast<std::streamsize>(size));
        if (m_stream)
            return size;

        return static_cast<u64>(m_stream.gcount());
    }

}