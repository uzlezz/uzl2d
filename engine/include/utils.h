#ifndef UZL2D_UTILS_H
#define UZL2D_UTILS_H

#include "common.h"
#include <vector>

namespace uz::utils
{

    void initVFS();

    bool loadFileFromData(std::string_view file_name, std::vector<u8>& data);

}

#endif //UZL2D_UTILS_H
