#pragma once

#include "common.h"
#include "renderer/vulkan/vulkan.h"
#include "renderer/vulkan/vk_mem_alloc.h"
#include <memory>
#include <string_view>
#include <vector>

namespace uz {

    class Device;

    struct TextureSpecification {
        u32 width;
        u32 height;
        u32 pixel_size = 4;
        VkFormat format = VK_FORMAT_R8G8B8A8_SRGB;
        VkFilter filter = VK_FILTER_NEAREST;
        VkSamplerAddressMode address_mode = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        bool bindless_register = true;

        const void* data;
    };

    class Texture2D {
    public:

        Texture2D() = default;
        ~Texture2D();

        static std::shared_ptr<Texture2D> create(Device& device, std::string_view path);
        static std::shared_ptr<Texture2D> create(Device& device, const TextureSpecification& spec);

        VkFormat getFormat() const { return m_format; }
        VkImageView getView() const { return m_view; }
        VkSampler getSampler() const { return m_sampler; }
        u32 getHandle() const { return m_handle; }

    private:

        Device* m_device;
        u32 m_handle{ 0 };
        VmaAllocation m_allocation;
        VkImage m_image;
        VkFormat m_format;
        VkImageView m_view;
        VkSampler m_sampler;
    };

    class BindlessTexturesManager {
    public:

        inline static constexpr u32 bindless_texture_binding = 0;

        BindlessTexturesManager(Device& device, u32 max_textures);
        ~BindlessTexturesManager();

        u32 registerTexture(VkImageView image_view, VkSampler sampler);
        void unregisterTexture(u32 handle);

    private:

        Device* m_device;
        u32 m_max_textures;

        std::shared_ptr<Texture2D> m_error_texture;
        std::vector<bool> m_slots;

        VkDescriptorPool m_descriptor_pool;
        VkDescriptorSetLayout m_descriptor_set_layout;
        VkDescriptorSet m_descriptor_set;
        VkPipelineLayout m_pipeline_layout;

    };

}
