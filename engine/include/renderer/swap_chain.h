#pragma once

#include <vector>

#include "common.h"
#include "renderer/vulkan/vulkan.h"

namespace uz
{

	class Device;

	struct SwapChainSupportDetails {
		VkSurfaceCapabilitiesKHR capabilities{};
		std::vector<VkSurfaceFormatKHR> formats;
		std::vector<VkPresentModeKHR> present_modes;
	};

	class SwapChain final {
	public:

		SwapChain(Device& device);
		~SwapChain();

		Device* getDevice() const { return m_device; }
		u32 getImageIndex() const { return m_image_index; }
		VkSwapchainKHR getSwapChain() const { return m_swap_chain; }
		VkImage getCurrentImage() const { return m_swap_chain_images[m_image_index]; }
		VkImageView getCurrentImageView() const { return m_swap_chain_image_views[m_image_index]; }
		VkFormat getFormat() const { return m_swap_chain_image_format; }
		VkExtent2D getExtent() const { return m_swap_chain_extent; }
		u32 getNumImages() const { return static_cast<u32>(m_swap_chain_images.size()); }

		VkImageView getImageView(u32 index) const { return m_swap_chain_image_views[index]; }

		void acquireNextImage(VkSemaphore semaphore);

		static SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR surface);

	private:

		Device* m_device;
		u32 m_image_index;
		VkSwapchainKHR m_swap_chain = VK_NULL_HANDLE;
		std::vector<VkImage> m_swap_chain_images;
		std::vector<VkImageView> m_swap_chain_image_views;
		VkFormat m_swap_chain_image_format;
		VkExtent2D m_swap_chain_extent;

	};

}