#pragma once

#include "renderer/renderer.h"

namespace uz
{

	struct GraphicsPipelineSpecification {
		std::string_view vertex;
		std::string_view fragment;

		std::string_view vertex_entry = "main";
		std::string_view fragment_entry = "main";

		bool msaa = false;
		bool alpha_blend = true;

		std::vector<VkDescriptorSetLayout> set_layouts;
		std::vector<VkFormat> color_attachments;
	};

	class GraphicsPipeline {
	public:

		GraphicsPipeline() = default;
		~GraphicsPipeline();

		static std::shared_ptr<GraphicsPipeline> create(
			Device& device, const GraphicsPipelineSpecification& spec);

		VkPipelineLayout getLayout() const { return m_layout; }
		VkPipeline getPipeline() const { return m_pipeline; }
		
	private:

		Device* m_device;
		VkPipelineLayout m_layout;
		VkPipeline m_pipeline;
	};

}
