#pragma once

#include <functional>
#include <string>
#include <vector>
#include <memory>
#include <optional>

#include "texture.h"
#include "../common.h"

struct GLFWwindow;

#include "renderer/vulkan/vulkan.h"
#include "renderer/vulkan/vk_mem_alloc.h"
#include "renderer/swap_chain.h"

namespace uz
{

    class Texture2D;
    class GraphicsPipeline;

    struct QueueFamilyIndices {
        std::optional<u32> graphics_family;
        std::optional<u32> present_family;
        std::optional<u32> compute_family;
    };

    struct DeviceProperties {
        std::string name;
        u16 max_texture2d_size;
    };

    constexpr u32 max_bindless_textures = 4096;

    struct Vertex {
        float position_x;
        float position_y;
        float uv_x;
        float uv_y;
    };

    class Device final {
    public:

        Device();
        ~Device();

        [[nodiscard]] GLFWwindow* window() const { return m_window; }
        [[nodiscard]] bool windowShouldClose() const;

        [[nodiscard]] const DeviceProperties& getProperties() const { return m_properties; }

        void drawFrame();

        std::shared_ptr<Texture2D> createTexture2D(std::string_view path);

        VkSurfaceKHR getSurface() const { return m_surface; }
        VkPhysicalDevice getPhysicalDevice() const { return m_physical_device; }
        VkDevice getDevice() const { return m_device; }
        VmaAllocator getAllocator() const { return m_allocator; }

        void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage,
            VmaAllocationCreateFlags flags, VmaMemoryUsage mem_usage, 
            VkMemoryPropertyFlags mem_props, VkBuffer& buffer, VmaAllocation& allocation);
        void createStagingBuffer(VkDeviceSize size, VkBuffer& buffer, VmaAllocation& allocation);

        void copyBuffer(VkBuffer src_buffer, VkDeviceSize src_offset,
            VkBuffer dst_buffer, VkDeviceSize dst_offset, VkDeviceSize size);
        void copyBufferToImage(VkBuffer buffer, VkDeviceSize buffer_offset, VkImage image, u32 width, u32 height);

        void transitionImageLayout(VkImage image, VkFormat format,
            VkImageLayout old_layout, VkImageLayout new_layout);

        void submitImmediately(const std::function<void(VkCommandBuffer)>& fn);

        u32 getNumFramesInFlight() const { return m_num_frames_in_flight; }
        std::vector<VkDescriptorSet>& getDescriptorSets() { return m_descriptor_sets; }
        BindlessTexturesManager& getTexturesManager() const { return *m_textures_manager; }

        void beginFrame();
        void endFrame();

        VkCommandBuffer getFrameCommandBuffer() const { return m_frame_command_buffers[m_frame_in_flight]; }
        u32 getCurrentFrameInFlight() const { return m_frame_in_flight; }
        VkExtent2D getSwapChainExtent() const { return m_swap_chain->getExtent(); }
        VkSampleCountFlagBits getMsaaSamples() const { return m_msaa_samples; }
        bool alphaToCoverageEnabled() const { return m_alpha_to_coverage; }

        static QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR surface);

    private:

        u32 m_num_frames_in_flight = 2;
        u32 m_frame_in_flight = 0;
        u32 m_swapchain_image_index = 0;
        u64 m_num_frames = 0;

        GLFWwindow* m_window;

        VkInstance m_instance;
        VkPhysicalDevice m_physical_device;
        VkDevice m_device;
        VmaAllocator m_allocator;

        std::unique_ptr<SwapChain> m_swap_chain;

        VkQueue m_graphics_queue = nullptr;
        VkQueue m_present_queue = nullptr;
        VkQueue m_compute_queue = nullptr;

        VkSurfaceKHR m_surface;

        DeviceProperties m_properties { "No Device" };

        VkDebugUtilsMessengerEXT m_debug_messenger;

        std::shared_ptr<GraphicsPipeline> m_graphics_pipeline;

        VkCommandPool m_command_pool;
        std::vector<VkCommandBuffer> m_frame_command_buffers;

        std::vector<VkSemaphore> m_image_available_semaphore;
        std::vector<VkSemaphore> m_render_finished_semaphore;
        std::vector<VkFence> m_in_flight_fence;

        VkSampleCountFlagBits m_msaa_samples = VK_SAMPLE_COUNT_1_BIT;
        bool m_alpha_to_coverage = false;

        VkDescriptorSetLayout m_descriptor_set_layout;
        VkDescriptorPool m_descriptor_pool;
        std::vector<VkDescriptorSet> m_descriptor_sets;
        std::unique_ptr<BindlessTexturesManager> m_textures_manager;

        void createInstance();
        bool checkValidationLayerSupport(const char* layer);
        void pickPhysicalDevice();
        void createLogicalDevice();
        void createAllocator();
        void createSurface();
        void createSwapChain();

        void createDescriptors();
        void setupBindlessDescriptors();
        void createGraphicsPipeline();

        void createCommandPool();
        void createCommandBuffers();
        void createSyncObjects();


        void beginRecordingCommandBuffer(VkCommandBuffer cmd_buffer);
        void endRecordingCommandBuffer(VkCommandBuffer cmd_buffer);
    };

}