#pragma once

#include <memory>

#include "common.h"
#include "renderer/vulkan/vulkan.h"
#include "renderer/vulkan/vk_mem_alloc.h"


namespace uz
{

	struct GpuBufferSpecification {
		u32 size;
		VkBufferUsageFlags usage;
		VmaMemoryUsage mem_usage;
		bool mapped = false;
		bool device_address = true;
	};

	class Device;

	class GpuBuffer final {
	public:

		static std::shared_ptr<GpuBuffer> create(Device& device,
			const GpuBufferSpecification& spec);

		GpuBuffer() = default;
		~GpuBuffer();

		void* getMappedData() const { return m_allocation_info.pMappedData; }
		VkDeviceSize getSize() const { return m_allocation_info.size; }
		VkDeviceAddress getDeviceAddress() const { return m_device_address; }

	private:

		Device* m_device;
		VkBuffer m_buffer;
		VmaAllocation m_allocation;
		VmaAllocationInfo m_allocation_info;
		VkDeviceAddress m_device_address{ 0 };
	};

}
