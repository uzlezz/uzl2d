#pragma once

#include <string_view>
#include <string>
#include "fixed_str.h"
#include "common.h"

namespace uz
{

    struct FileInfo
    {
        FileInfo() = default;
        ~FileInfo() = default;
        FileInfo(std::string_view path);
        FileInfo(std::string_view base_path, std::string_view name, bool is_directory);

        void init(std::string_view base_path, std::string_view name, bool is_directory);

        std::string_view getName() const { return static_cast<std::string_view>(m_name); }
        std::string_view getNameWithoutExtension() const { return static_cast<std::string_view>(m_base_name); }
        std::string_view getExtension() const;
        std::string_view getAbsolutePath() const { return static_cast<std::string_view>(m_absolute_path); }
        std::string_view getParentDirectory() const { return static_cast<std::string_view>(m_base_path); }

        bool isDirectory() const { return m_is_directory; }
        bool isValid() const;

    private:

        FixedStr<64> m_name;
        FixedStr<64> m_base_name;
        FixedStr<255> m_absolute_path;
        FixedStr<191> m_base_path;
        u8 m_extension_offset{255};
        b8 m_is_directory : 1{false};
    };

    inline bool operator==(const FileInfo &lhs, const FileInfo &rhs) {
        return lhs.getAbsolutePath() == rhs.getAbsolutePath();
    }

    inline bool operator<(const FileInfo &lhs, const FileInfo &rhs) {
        return lhs.getAbsolutePath() < rhs.getAbsolutePath();
    }

}