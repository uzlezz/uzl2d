#pragma once

#include "fs/file_system.h"
#include "fs/file.h"
#include <unordered_map>
#include <list>
#include <utility>

namespace uz {

    class VirtualFileSystem final {
    public:

        VirtualFileSystem() = default;

        ~VirtualFileSystem();

        void mount(std::string_view alias, const IFileSystem::Ptr& fs);

        void unmount(std::string_view alias);

        bool isFileSystemExists(std::string_view alias) const;

        IFileSystem::Ptr getFileSystem(std::string_view alias);

        IFile::Ptr openFile(const FileInfo &info, IFile::OpenMode mode);

        void closeFile(IFile::Ptr file);

    private:

        struct FileSystemDescriptor {
            std::string alias;
            IFileSystem::Ptr file_system;

            FileSystemDescriptor(std::string_view a, IFileSystem::Ptr fs)
                    : alias(a), file_system(std::move(fs)) {}
        };

        std::unordered_map<std::string, IFileSystem::Ptr> m_file_system;
        std::list<FileSystemDescriptor> m_descriptors;
        std::unordered_map<uintptr_t, IFileSystem::Ptr> m_opened_files;

    };

}