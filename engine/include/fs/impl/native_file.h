#pragma once

#include "fs/file.h"
#include <fstream>

namespace uz {

    class NativeFile final : public IFile {
    public:

        NativeFile(const FileInfo &info);

        ~NativeFile() override;

        const FileInfo &getFileInfo() const override { return m_info; }

        u64 getSize() override;

        bool isReadOnly() const override { return m_is_read_only; }

        void open(OpenMode mode) override;

        void close() override;

        bool isOpened() const override;

        u64 seek(u64 offset, SeekOrigin origin) override;

        u64 tell() override;

        u64 read(u8 *buffer, u64 size) override;

        u64 write(const u8 *buffer, u64 size) override;

    private:

        FileInfo m_info;
        std::fstream m_stream;
        b8 m_is_read_only: 1;
        OpenMode m_mode;
    };

}