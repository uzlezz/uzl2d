#pragma once

#include "fs/file_system.h"

namespace uz {

    class NativeFileSystem final : public IFileSystem {
    public:

        NativeFileSystem(std::string_view path);

        ~NativeFileSystem() override;

        void init() override;

        void shutdown() override;

        bool isInitialized() const override { return m_is_initialized; }

        std::string_view getBasePath() const override { return m_base_path; }

        const FileList &getFileList() const override { return m_file_list; }

        bool isReadOnly() const override;

        IFile::Ptr openFile(const FileInfo &info, IFile::OpenMode mode) override;

        void closeFile(IFile::Ptr file) override;

        bool createFile(const FileInfo &info) override;

        bool removeFile(const FileInfo &info) override;

        bool copyFile(const FileInfo &src, const FileInfo &dst) override;

        bool renameFile(const FileInfo &src, const FileInfo &dst) override;

        bool exists(const FileInfo &info) const override;

        bool isFile(const FileInfo &info) const override;

        bool isDirectory(const FileInfo &info) const override;

    private:

        std::string m_base_path;
        b8 m_is_initialized: 1;
        FileList m_file_list;

        IFile::Ptr findFile(const FileInfo &info) const;

        void buildFileList();
    };

}