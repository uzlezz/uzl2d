#pragma once

#include "fs/file.h"
#include <vector>

namespace uz {

    class MemoryFile final : public IFile {
    public:

        MemoryFile(const FileInfo &info);

        ~MemoryFile();

        const FileInfo &getFileInfo() const override { return m_info; }

        u64 getSize() override;

        bool isReadOnly() const override { return m_is_read_only; }

        void open(OpenMode mode) override;

        void close() override;

        bool isOpened() const override;

        u64 seek(u64 offset, SeekOrigin origin) override;

        u64 tell() override;

        u64 read(u8 *buffer, u64 size) override;

        u64 write(const u8 *buffer, u64 size) override;

    private:

        std::vector<u8> m_data;
        FileInfo m_info;
        b8 m_is_read_only: 1;
        b8 m_is_opened: 1;
        u64 m_seek_pos;
        OpenMode m_mode;

    };

}