#pragma once

#include "fs/file_info.h"
#include <memory>

namespace uz {

    class IFile {
    public:

        using Ptr = std::shared_ptr<IFile>;

        enum class SeekOrigin {
            Begin, End, Set
        };

        enum OpenMode {
            None = 0, In = 0x01, Out = 0x02, ReadWrite = In | Out, Append = 0x04, Truncate = 0x08
        };

        IFile() = default;

        virtual ~IFile() = default;

        virtual const FileInfo &getFileInfo() const = 0;

        virtual u64 getSize() = 0;

        [[nodiscard]] virtual bool isReadOnly() const = 0;

        virtual void open(OpenMode mode) = 0;

        virtual void close() = 0;

        [[nodiscard]] virtual bool isOpened() const = 0;

        virtual u64 seek(u64 offset, SeekOrigin origin) = 0;

        virtual u64 tell() = 0;

        virtual u64 read(u8 *buffer, u64 size) = 0;

        virtual u64 write(const u8 *buffer, u64 size) = 0;

        template<class T>
        bool read(T &value) {
            return read(&value, sizeof(value)) == sizeof(value);
        }

        template<class T>
        u64 write(const T &value) {
            return write(&value, sizeof(value)) == sizeof(value);
        }

    };

    inline bool operator==(const IFile &lhs, const IFile &rhs) {
        return lhs.getFileInfo() == rhs.getFileInfo();
    }

    inline bool operator==(const IFile::Ptr& lhs, const IFile::Ptr& rhs) {
        return lhs && rhs && lhs->getFileInfo() == rhs->getFileInfo();
    }

}