#pragma once

#include "fs/file.h"
#include <set>

namespace uz {

    class IFileSystem {
    public:

        using Ptr = std::shared_ptr<IFileSystem>;
        using FileList = std::set<IFile::Ptr>;

        IFileSystem() = default;

        virtual ~IFileSystem() = default;

        virtual void init() = 0;

        virtual void shutdown() = 0;

        [[nodiscard]] virtual bool isInitialized() const = 0;

        [[nodiscard]] virtual std::string_view getBasePath() const = 0;

        [[nodiscard]] virtual const FileList &getFileList() const = 0;

        [[nodiscard]] virtual bool isReadOnly() const = 0;

        virtual IFile::Ptr openFile(const FileInfo &info, IFile::OpenMode mode) = 0;

        virtual void closeFile(IFile::Ptr file) = 0;

        virtual bool createFile(const FileInfo &info) = 0;

        virtual bool removeFile(const FileInfo &info) = 0;

        virtual bool copyFile(const FileInfo &src, const FileInfo &dst) = 0;

        virtual bool renameFile(const FileInfo &src, const FileInfo &dst) = 0;

        [[nodiscard]] virtual bool exists(const FileInfo &info) const = 0;

        [[nodiscard]] virtual bool isFile(const FileInfo &info) const = 0;

        [[nodiscard]] virtual bool isDirectory(const FileInfo &info) const = 0;

    };

}