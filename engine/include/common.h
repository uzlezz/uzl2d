#ifndef VOXEL_ENGINE_COMMON_H
#define VOXEL_ENGINE_COMMON_H

#include <cstdint>
#include <string_view>

namespace uz
{

    using i8 = std::int8_t;
    using i16 = std::int16_t;
    using i32 = std::int32_t;
    using i64 = std::int64_t;
    using u8 = std::uint8_t;
    using u16 = std::uint16_t;
    using u32 = std::uint32_t;
    using u64 = std::uint64_t;
    using b8 = u8;

#if defined(_DEBUG) || !defined(NDEBUG)
    constexpr bool debug = true;

#ifndef _WIN32
    constexpr std::string_view data_dir = "/home/gleb/projects/c++/uzl2d/data";
#else
    constexpr std::string_view data_dir = "P:/C++/uzl2d/data";
#endif
#else
    constexpr bool debug = false;

    constexpr std::string_view data_dir = "data";
#endif

    extern class VirtualFileSystem* vfs;

}

#endif //VOXEL_ENGINE_COMMON_H
