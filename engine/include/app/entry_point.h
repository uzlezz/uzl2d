#pragma once

extern int appEntry(int argc, char** argv);

#if defined(_WIN32) || _WIN32

#if !defined(UZ_DISTRIBUTION)

int main(int argc, char** argv) {
	return appEntry(argc, argv);
}

#else

#include <windows.h>
int WINAPI WinMain(HINSTANCE instance, HINSTANCE prev_instance, PSTR cmd_line, int cmd_show) {
	int num_args;
	auto wargs = CommandLineToArgvW(GetCommandLineW(), &num_args);

	char** args = new char* [num_args];

	for (int i = 0; i < num_args; ++i) {
		auto wsize = static_cast<int>(wcslen(wargs[i]));
		auto size_needed = WideCharToMultiByte(CP_UTF8, 0, wargs[i], wsize, 
			nullptr, 0, nullptr, nullptr);

		args[i] = new char[size_needed + 1];
		WideCharToMultiByte(CP_UTF8, 0, wargs[i], wsize,
			args[i], size_needed, nullptr, nullptr);
		args[i][size_needed] = '\0';
	}
	LocalFree(wargs);

	int ret = appEntry(num_args, args);

	for (int i = 0; i < num_args; ++i)
	{
		delete[] args[i];
	}
	delete[] args;
	return ret;
}

#endif

#else

int main(int argc, char** argv) {
	return appEntry(argc, argv);
}

#endif