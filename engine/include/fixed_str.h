#pragma once

#include "common.h"
#include <array>
#include <string_view>
#include <cstring>

namespace uz
{

	template <u64 MaxLength = 32>
	class FixedStr
	{
	public:

		inline static constexpr u64 max_length = MaxLength;
        inline static constexpr u64 npos = ~(0ULL);

        FixedStr() : m_length(0) {
            std::memset(m_data.data(), '\0', max_length + 1);
		}

        FixedStr(const char* str) {
			*this = str;
		}

        explicit FixedStr(std::string_view str) {
			*this = str;
		}

        [[nodiscard]] u64 getSize() const { return m_length; }
        [[nodiscard]] u64 length() const { return m_length; }
		[[nodiscard]] const char* data() const { return m_data.data(); }
		char* data() { return m_data.data(); }

		auto begin() { return m_data.begin(); }
		auto end() { return m_data.end(); }
		const auto begin() const { return m_data.begin(); }
		const auto end() const { return m_data.end(); }

        FixedStr& operator=(std::string_view str) {
			//u64 size = str.size() > max_length ? max_length : str.size();

            u64 size = strlen(str.data());
            size = size > str.length() ? str.length() : size;
            size = size > max_length ? max_length : size;
            if (size > 0)
				std::memcpy(m_data.data(), str.data(), size);
			m_length = size;
			m_data[size] = '\0';
			return *this;
		}

        FixedStr& operator=(const char* str) {
			*this = std::string_view(str);
            return *this;
		}

        explicit operator std::string_view() const {
            return std::string_view(m_data.begin(), m_data.end());
        }

        void resize(u64 size) {
            m_data[size] = '\0';
            m_length = size;
        }

        [[nodiscard]] bool startsWith(char c) const
        { return m_length > 0 &&  m_data[0] == c; }

        [[nodiscard]] bool endsWith(char c) const
        { return m_length > 0 &&  m_data[m_length - 1] == c; }

        FixedStr<max_length>& operator+=(char c) {
            if (m_length == max_length) return *this;

            m_data[m_length++] = c;
            m_data[m_length] = '\0';
            return *this;
        }

        std::string_view substr(u64 from, u64 size = (~0ULL)) {
            if (from >= m_length) return "";

            auto count = m_length - from;
            if (count >= size) count = size;

            return std::string_view(begin() + from, begin() + from + count);
        }

        template <u64 L>
        auto operator+(const FixedStr<L>& other) -> FixedStr<MaxLength + L> {
            char str[MaxLength + L + 1];
            std::memcpy(str, m_data.data(), m_length);
            std::memcpy(str + m_length, other.data(), other.length());
            str[m_length + other.length()] = '\0';
            return str;
        }

        [[nodiscard]] u64 rfind(char c) const {
            for (u64 i = m_length - 1; i > 0; --i) {
                if (m_data[i] == c) return i;
            }
            return m_data[0] == c ? 0 : npos;
        }

        template <u64 L>
        bool operator==(const FixedStr<L>& other) const {
            return m_length == other.length() && std::memcmp(data(), other.data(), m_length) == 0;
        }

	private:

		std::array<char, max_length + 1> m_data;
		u64 m_length{};
	};

}