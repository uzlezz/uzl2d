#version 450 core

layout (location = 0) out vec4 color;

layout (location = 0) in vec2 uv0;

layout (binding = 0) uniform sampler2D tex;

void main() {
    color = texture(tex, uv0);
}
