#version 450
#extension GL_EXT_nonuniform_qualifier : enable

layout (set = 0, binding = 0) uniform sampler2D textures2D[];

layout (location = 0) out vec4 color;

layout (location = 0) in vec2 uv;

void main()
{
    color = texture(textures2D[0], uv);
}