%flags%="--target-env vulkan1.3"

mkdir bin

glslc triangle.vert %flags% -o bin/triangle.vert.spv
glslc triangle.frag %flagS% -o bin/triangle.frag.spv

glslc textured_triangle.vert %flags% -o bin/textured_triangle.vert.spv
glslc textured_triangle.frag %flagS% -o bin/textured_triangle.frag.spv

glslc bindless_triangle.vert %flags% -o bin/bindless_triangle.vert.spv
glslc bindless_triangle.frag %flagS% -o bin/bindless_triangle.frag.spv
pause