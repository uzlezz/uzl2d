#version 450
#extension GL_EXT_buffer_reference : require

layout (location = 0) out vec2 uv;

struct Vertex
{
    vec2 position;
    vec2 uv;
}; 

layout (buffer_reference, std430) readonly buffer VertexBuffer
{ 
	Vertex vertices[];
};

layout (push_constant) uniform constants
{	
	mat4 render_matrix;
	VertexBuffer vertex_buffer;
} PushConstants;

void main() 
{	
	Vertex v = PushConstants.vertex_buffer.vertices[gl_VertexIndex];

	gl_Position = PushConstants.render_matrix * vec4(v.position, 0.0, 1.0);
	uv = v.uv;
}