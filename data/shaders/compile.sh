$flags=

mkdir bin

glslc triangle.vert -o bin/triangle.vert.spv
glslc triangle.frag -o bin/triangle.frag.spv

glslc textured_triangle.vert -o bin/textured_triangle.vert.spv
glslc textured_triangle.frag -o bin/textured_triangle.frag.spv

glslc bindless_triangle.vert -o bin/bindless_triangle.vert.spv
glslc bindless_triangle.frag -o bin/bindless_triangle.frag.spv