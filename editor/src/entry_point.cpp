#include <iostream>
#include <memory>
#include <thread>
#include <chrono>
#include "renderer/renderer.h"
#include "utils.h"
#include "fs/virtual_file_system.h"
#include "fs/impl/native_file_system.h"
#include <GLFW/glfw3.h>

#include "renderer/texture.h"
#include "app/entry_point.h"

int appEntry(int argc, char** argv) {
    uz::utils::initVFS();

    std::unique_ptr<uz::Device> device;

    {
#if 0
        uz::vfs->mount("/shaders",
                       std::make_shared<uz::NativeFileSystem>(
                               std::string(uz::data_dir) + "/shaders/bin")
        );
#else
        uz::vfs->mount("/shaders", std::make_shared<uz::NativeFileSystem>(std::string("data\\shaders\\bin")));
        uz::vfs->mount("/textures", std::make_shared<uz::NativeFileSystem>(std::string("data\\textures")));
#endif
    }

    try {
        device = std::make_unique<uz::Device>();
    } catch (std::runtime_error& e) {
        std::cerr << e.what() << "\n\nTerminating";
        return 1;
    }

    std::cout << "Max texture2d size: " << device->getProperties().max_texture2d_size << "\n";

    auto tex = device->createTexture2D("/textures/Palm1.png");

    for (uz::u32 i = 0; i < device->getNumFramesInFlight(); ++i) {
        VkDescriptorImageInfo image_info{};
        image_info.imageView = tex->getView();
        image_info.sampler = tex->getSampler();
        image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        std::array<VkWriteDescriptorSet, 1> descriptor_writes;

        descriptor_writes[0] = { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET };
        descriptor_writes[0].dstSet = device->getDescriptorSets()[i];
        descriptor_writes[0].dstBinding = 0;
        descriptor_writes[0].dstArrayElement = 0;
        descriptor_writes[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptor_writes[0].descriptorCount = 1;
        descriptor_writes[0].pImageInfo = &image_info;

        vkUpdateDescriptorSets(device->getDevice(), static_cast<uz::u32>(descriptor_writes.size()), descriptor_writes.data(), 0, nullptr);
    }

    while (!device->windowShouldClose()) {
        int x, y;
        glfwGetWindowPos(device->window(), &x, &y);
        if (glfwGetKey(device->window(), GLFW_KEY_D) == GLFW_PRESS) {
            x += 3;
        }
        if (glfwGetKey(device->window(), GLFW_KEY_A) == GLFW_PRESS) {
            x -= 3;
        }
        if (glfwGetKey(device->window(), GLFW_KEY_W) == GLFW_PRESS) {
            y -= 3;
        }
        if (glfwGetKey(device->window(), GLFW_KEY_S) == GLFW_PRESS) {
            y += 3;
        }
        glfwSetWindowPos(device->window(), x, y);

        glfwPollEvents();



        device->beginFrame();
        vkCmdDraw(device->getFrameCommandBuffer(), 3, 1, 0, 0);
        device->endFrame();
    }

    delete uz::vfs;

    return 0;
}
