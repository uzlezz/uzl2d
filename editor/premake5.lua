project "editor"
    kind "ConsoleApp"
    language "C++"

    targetdir (bin_dir)
    objdir (obj_dir .. "%{prj.name}")

    files
    {
        "include/**.h",
        "src/**.cpp"
    }

    includedirs
    {
        "include",
        headers["glm"],
        headers["engine"],
        headers["glfw"],
        headers["stb"]
    }

    links
    {
        "engine",
        "GLFW",
        "stb"
    }